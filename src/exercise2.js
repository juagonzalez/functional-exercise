const faverage = numbers => {

	if (numbers === undefined || numbers.length == 0)
		return 0;

	const sum = numbers.reduce(
		function (total, currentValue) {
			return total + currentValue;
		}, 0);
	const len = numbers.length;

	return (sum/len);
};

module.exports = faverage;
