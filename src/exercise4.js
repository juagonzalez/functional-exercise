const wordCount = sequence => {

	if (sequence === undefined || sequence.length == 0)
		return null;
	
	words = sequence.split(' ')
	words = words.filter(
		function (word) {
			if (word !== '')
				return true;
		});
	words = words.map(word => word.toLowerCase());
	
	if (words.length == 0)
		return null;

	counts = words.reduce(
		function(count, word) {
			count[word] = count[word] ? count[word] + 1 : 1;
			return count;
		}, {});

	return counts
};

module.exports = {
	wordCount,
};
