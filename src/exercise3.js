/*
 * Returns a function that approximates the derive of fn with error h in the value given to the returned function.
 */
const fderive = (fn, h) => {

	function estimate(x){

		a = fn(x - h)
		b = fn(x + h)
		
		return (b - a) / (2 * h)
	}

	return estimate;
};

const squear = x => {

	return x*x;
};

module.exports = {
  fderive,
  squear,
};
